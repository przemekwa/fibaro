﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibaroTask
{
    class Info
    {
        public string SerialNumber { get; set; }
        public string SoftVersion { get; set; }
        public string Mac { get; set; }

        public override string ToString()
        {
            return string.Format("SerialNumber: {0} SoftVersion: {1} Mac: {2}", this.SerialNumber, this.SoftVersion, this.Mac);
        }
    }


    class Section
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0} Name: {1}", this.Id, this.Name);
        }

    }

    class Room
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SectionID {get;set;}
        public int SortOrder { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0} Name: {1} SectionID: {2}", this.Id, this.Name, this.SectionID);
        }
    }

    class Device
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int roomID { get; set; }
        public string type { get; set; }
        public List<Properties> Properties { get; set; }
        public List<Action> Action { get; set; }
        public int SortOrder { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0} Name: {1} Type: {2} Value: {3}", this.Id, this.Name, this.type, this.Properties.Count == 0 ? string.Empty : this.Properties[0].value);
        }
    }


     class Properties
    {
        public int dead { get; set; }
        public string disabled { get; set; }
        public string value { get; set; }
      
    }

     class Action
    {
        public int setValue { get; set; }
        public int turnOff { get; set; }
        public int turnOn { get; set; }
    }


     class State
     {
         public string Status { get; set; }
         public int last { get; set; }
         public List<Change> changes { get; set; }

         public override string ToString()
         {
             return string.Format("Status: {0} LastID: {1} Change: {2}", this.Status, this.last, this.changes.Count == 0 ? string.Empty : this.changes[0].Log);
         }
     }
    class Change
    {
        public int Id {get;set;}
        public string Log {get;set;}
        public int Value {get;set;}
        public string ValueSensor {get;set;}
    }
}
