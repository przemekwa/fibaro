﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Threading;

namespace FibaroTask
{
    class Program
    {
        static RestClient restClient;

        static Program()
        {
            restClient = new RestClient("http://217.168.133.114:9999");
            restClient.Authenticator = new HttpBasicAuthenticator("admin", "admin");
        }

        static void Main(string[] args)
        {

            Task refreshTask = new Task(() =>
            {
                for (; ; )
                { 
                 var lastId = FibaroRequest<State>("/api/refreshStates?last=0").last;
                 var state = FibaroRequest<State>(string.Format("/api/refreshStates?last={0}", lastId));

                 Console.WriteLine();
                Console.WriteLine("----------refreshTask-----------");
                Console.WriteLine(state);

                Thread.Sleep(2000);
                }
            });


            refreshTask.Start();

            var info = FibaroRequest<Info>("/api/settings/info");
            var sections = FibaroRequest<List<Section>>("/api/sections");
            var rooms = FibaroRequest<List<Room>>("/api/rooms");
            var devices = FibaroRequest<List<Device>>("/api/devices").Where(d => d.type == "binary_light" || d.type == "dimmable_light");
            
           ShowDate(sections, rooms, devices, info);


           CallActionBinaryLight(4, "turnOff");

           Console.ReadKey();

        }


        static void ShowDate(IEnumerable<Section> sections, IEnumerable<Room> rooms, IEnumerable<Device> devices, Info info)
        {
            var sectionsView = from section in sections
                               join room in rooms on section.Id equals room.SectionID
                               into roomsView
                               select new
                               {
                                   Name = section.Name,
                                   Id = section.Id,
                                   Rooms = from room in roomsView
                                           join device in devices on room.Id equals device.roomID
                                           into devicesView
                                           select new
                                           {
                                               Id = room.Id,
                                               Name = room.Name,
                                               Devices = devicesView
                                           }
                               };




            Console.WriteLine("---------info-------------");
            Console.WriteLine(info);

            foreach (var section in sectionsView)
            {
                Console.WriteLine("\n\n");
                Console.WriteLine("---------Section-------------");
                Console.WriteLine(string.Format("Id: {0} Name: {1}", section.Id, section.Name));

                foreach (var room in section.Rooms)
                {
                    Console.WriteLine("---------Room-------------");
                    Console.WriteLine(string.Format("  Id: {0} Name: {1}", room.Id, room.Name));

                    Console.WriteLine("---------Devices-------------");
                    foreach (var device in room.Devices)
                    {
                        Console.WriteLine(string.Format("    {0}", device));
                    }
                }
            }
        }

        static T FibaroRequest<T>(string Uri) where T: new()
        {
            var request = new RestRequest(Uri);

            return (T)restClient.Execute<T>(request).Data;
        }

        static void CallActionBinaryLight(int deviceID, string arg )
        {
            var callAction = "/api/callAction?deviceID={id}&name={name}";

            var callRequest = new RestRequest(callAction);

            callRequest.AddUrlSegment("id", deviceID.ToString());
            callRequest.AddUrlSegment("name", arg);

            var test = restClient.Execute(callRequest).Content;
        }
        
    }
}
